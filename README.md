# Wargames
[![pipeline status](https://gitlab.com/Jakkar/IDATG2001-Project/badges/main/pipeline.svg)](https://gitlab.com/Jakkar/IDATG2001-Project/-/pipelines) [![coverage report](https://gitlab.com/Jakkar/IDATG2001-Project/badges/main/coverage.svg)](https://jakkar.gitlab.io/IDATG2001-Project/coverage/) 

Wargames is a JavaFX application for creating armies and simulating battles between them. 

Check out the [screenshots](docs/screenshots/README.md)!

## Requirements
* Java 11 or higher
* Maven

## Running

### Directly with Maven

```Bash
mvn javafx:run
```

### Packaging to JAR

```Bash
mvn package
```

After packaging, the JAR can be found in `target/` as `wargames-0.1-shaded.jar`.

## Code documentation

Javadoc documentation can be found on GitLab pages:

-  [Project documentation](https://jakkar.gitlab.io/IDATG2001-Project/javadoc/apidocs/)
-  [Test documentation](https://jakkar.gitlab.io/IDATG2001-Project/javadoc/testapidocs/)

A detailed coverage report can also be found on GitLab pages:
- [Coverage](https://jakkar.gitlab.io/IDATG2001-Project/coverage/)

## Project related material

- [Wireframe](docs/documents/wireframe.pdf)

Project description (in Norwegian):
- [Project Description - Part 1](docs/documents/IDATx2001%20-%20Wargames%20-%20Del%201.pdf)
- [Project Description - Part 2](docs/documents/IDATx2001%20-%20Wargames%20-%20Del%202.pdf)
- [Project Description - Part 3](docs/documents/IDATx2001%20-%20Wargames%20-%20Del%203.pdf)