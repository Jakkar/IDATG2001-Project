# Screenshots

## Home Page

![Screenshot of application homepage](home.png)

## Create New Army Page

![Screenshot of create new army page](create_new_army.png)

## Start New War Page

![Screenshot of start new war page](start_new_war.png)

## Battle Page

![Screenshot of battle page](battle.png)

## Result page

![Screenshot of results oage](result.png)

