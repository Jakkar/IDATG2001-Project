package no.jakkar.wargames.model.army;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import no.jakkar.wargames.model.units.CavalryUnit;
import no.jakkar.wargames.model.units.CommanderUnit;
import no.jakkar.wargames.model.units.InfantryUnit;
import no.jakkar.wargames.model.units.RangedUnit;
import no.jakkar.wargames.model.units.Unit;
import no.jakkar.wargames.model.units.UnitFactory;

/**
 * Class for testing armies and functionality of armies.
 * 
 * @see Army
 */
public class ArmyTest {

    private List<Unit> testUnitList;

    @BeforeEach
    void populateTestUnitList() {
        Unit cavalryUnit = UnitFactory.getInstance().getUnit("CavalryUnit", "CavalryUnit", 100);
        Unit commanderUnit = UnitFactory.getInstance().getUnit("CommanderUnit", "CommanderUnit", 100);
        Unit infantryUnit = UnitFactory.getInstance().getUnit("InfantryUnit", "InfantryUnit", 100);
        Unit rangedUnit = UnitFactory.getInstance().getUnit("RangedUnit", "RangedUnit", 100);

        testUnitList = new ArrayList<>(Arrays.asList(cavalryUnit, commanderUnit, infantryUnit, rangedUnit));
    }

    @Test
    @DisplayName("Test if object creation works with constructor.")
    void testConstructor() {
        Army army = new Army("Army");

        assertEquals("Army", army.getName());
    }

    @Test
    @DisplayName("Test if object creation works with constructor that also adds units.")
    void testConstructorWithProvidedUnits() {
        Army army = new Army("army", testUnitList);

        assertEquals(testUnitList, army.getAllUnits());
    }

    @Test
    @DisplayName("Test if adding a unit to the army works.")
    void testAdd() {
        Army army = new Army("Army");
        Unit unit = UnitFactory.getInstance().getUnit("CavalryUnit", "Unit", 100);

        army.add(unit);

        assertEquals(unit, army.getAllUnits().get(0));
    }

    @Test
    @DisplayName("Test if adding a list of units works.")
    void testAddAll() {
        Army army = new Army("Army");

        army.addAll(testUnitList);

        assertEquals(testUnitList, army.getAllUnits());
    }

    @Test
    @DisplayName("Test if getting all units works.")
    void testGetAllUnits() {
        Army army = new Army("Army");

        army.addAll(testUnitList);

        assertEquals(4, army.getAllUnits().size());
    }

    @Test
    @DisplayName("Test if getting name of army works.")
    void testGetName() {
        Army army = new Army("Army");

        assertEquals("Army", army.getName());
    }

    @Test
    @DisplayName("Test if removing unit from army works.")
    void testRemove() {
        Army army = new Army("Army");
        army.addAll(testUnitList);

        Unit unit = army.getRandom();

        assertTrue(army.getAllUnits().contains(unit));

        army.remove(unit);

        assertFalse(army.getAllUnits().contains(unit));
    }

    @Test
    @DisplayName("Test if checking if army has units works.")
    void testHasUnits() {
        Army armyWithUnits = new Army("Army", testUnitList);
        Army armyEmpty = new Army("Army");

        assertFalse(armyEmpty.hasUnits());
        assertTrue(armyWithUnits.hasUnits());
    }

    @Test
    @DisplayName("Test if getting units by type works.")
    void testGetUnitsByType() {
        Army armyWithUnits = new Army("Army", testUnitList);

        List<Unit> filtered_army = armyWithUnits.getUnitsByType(CommanderUnit.class);

        assertInstanceOf(CommanderUnit.class, filtered_army.get(0));
    }

    @Test
    @DisplayName("Test if getting only cavalry units works.")
    void testGetCavalryUnits() {
        Army armyWithUnits = new Army("Army", testUnitList);

        List<Unit> filtered_army = armyWithUnits.getCavalryUnits();

        assertInstanceOf(CavalryUnit.class, filtered_army.get(0));
    }

    @Test
    @DisplayName("Test if getting only commander units works.")
    void testGetCommanderUnits() {
        Army armyWithUnits = new Army("Army", testUnitList);

        List<Unit> filtered_army = armyWithUnits.getCommanderUnits();

        assertInstanceOf(CommanderUnit.class, filtered_army.get(0));
    }

    @Test
    @DisplayName("Test if getting only infantry units works.")
    void testGetInfantryUnits() {
        Army armyWithUnits = new Army("Army", testUnitList);

        List<Unit> filtered_army = armyWithUnits.getInfantryUnits();

        assertInstanceOf(InfantryUnit.class, filtered_army.get(0));
    }

    @Test
    @DisplayName("Test if getting only ranged units works.")
    void testGetRangedUnits() {
        Army armyWithUnits = new Army("Army", testUnitList);

        List<Unit> filtered_army = armyWithUnits.getRangedUnits();

        assertInstanceOf(RangedUnit.class, filtered_army.get(0));
    }

    @Test
    @DisplayName("Test if overridden equals and hashCode functions correctly compares two objects")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple().forClass(Army.class).verify();
    }
}
