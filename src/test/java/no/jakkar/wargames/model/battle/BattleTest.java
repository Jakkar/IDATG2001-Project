package no.jakkar.wargames.model.battle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.units.UnitFactory;

/**
 * Class for testing functionality of battles.
 * 
 * @see Battle
 */
public class BattleTest {

    private Army testArmyWeak = new Army("Weak Army");
    private Army testArmyStrong = new Army("Strong Army");

    @BeforeEach
    void populateTestArmies() {
        testArmyWeak.add(UnitFactory.getInstance().getUnit("CommanderUnit", "CommanderUnit", 15));
        testArmyWeak.add(UnitFactory.getInstance().getUnit("CavalryUnit", "CavalryUnit", 10));
        testArmyWeak.add(UnitFactory.getInstance().getUnit("RangedUnit", "RangedUnit", 5));

        testArmyStrong.addAll(UnitFactory.getInstance().getUnits("CommanderUnit", 10, "CommanderUnit", 20));
        testArmyStrong.addAll(UnitFactory.getInstance().getUnits("CavalryUnit", 10, "CavalryUnit", 15));
        testArmyStrong.addAll(UnitFactory.getInstance().getUnits("RangedUnit", 10, "RangedUnit", 10));
        testArmyStrong.addAll(UnitFactory.getInstance().getUnits("InfantryUnit", 10, "InfantryUnit", 10));
    }

    @Test
    @DisplayName("Test if object creation works with constructor.")
    void testConstructor() {
        Battle battle = new Battle(testArmyWeak, testArmyStrong, null);

        assertEquals(battle.getArmyOne(), testArmyWeak);
        assertEquals(battle.getArmyTwo(), testArmyStrong);
    }

    @Test
    @DisplayName("Test if a battle correctly simulates and declares a winner.")
    void testSimulate() {
        Battle battle = new Battle(testArmyWeak, testArmyStrong, null);

        // check state before simulation
        try {
            battle.getBattleStates();
            battle.isBattleSimulated();

            assert false;
        } catch (Exception e) {
            assert true;
        }

        // in reality there should be no chance of weak army winning,
        // so this should be fine :)
        battle.simulate();

        assertEquals(testArmyStrong, battle.getVictoriousArmy());

        // the battle state should also have updated now
        assert !battle.getBattleStates().isEmpty();
        assert battle.isBattleSimulated();
    }

    @Test
    @DisplayName("Test if overridden equals and hashCode functions correctly compares two objects")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple().forClass(Battle.class).verify();
    }

    @Test
    @DisplayName("Test if battle reset properly resets battle")
    void testReset() {
        Battle battle = new Battle(testArmyWeak.makeCopy(), testArmyStrong.makeCopy(), null);

        // check state before simulation
        assertEquals(testArmyWeak, battle.getArmyOne());
        assertEquals(testArmyStrong, battle.getArmyTwo());
        assert !battle.isBattleSimulated();
        try {
            battle.getVictoriousArmy();
            battle.getBattleStates();

            assert false;
        } catch (Exception e) {
            assert true;
        }

        battle.simulate();

        // make sure state has changed
        assertNotEquals(testArmyWeak, battle.getArmyOne());
        assert battle.isBattleSimulated();
        try {
            battle.getVictoriousArmy();
            battle.getBattleStates();

            assert true;
        } catch (Exception e) {
            assert false;
        }

        battle.reset();

        // check state is back to original
        assertEquals(testArmyWeak, battle.getArmyOne());
        assertEquals(testArmyStrong, battle.getArmyTwo());
        assert !battle.isBattleSimulated();
        try {
            battle.getVictoriousArmy();
            battle.getBattleStates();

            assert false;
        } catch (Exception e) {
            assert true;
        }

    }

    @Test
    @DisplayName("Make sure battle can't be simulated twice without reset")
    void testRepeatedSimulation() {
        Battle battle = new Battle(testArmyWeak, testArmyStrong, null);

        battle.simulate();

        try {
            battle.simulate();

            assert false;
        } catch (Exception e) {
            assert true;
        }
    }
}
