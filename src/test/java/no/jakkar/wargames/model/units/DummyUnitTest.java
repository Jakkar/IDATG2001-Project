package no.jakkar.wargames.model.units;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

/**
 * Class for testing units and their common functionality.
 */
public class DummyUnitTest {

    @Test
    @DisplayName("Assure health can not go below 0.")
    void testHealthCanNotGoBelowZero() {
        DummyUnit attacker = new DummyUnit("Unit", 100, 10, 0);
        DummyUnit defender = new DummyUnit("Unit", 10);

        attacker.attack(defender, null);
        attacker.attack(defender, null);
        attacker.attack(defender, null);

        assertEquals(0, defender.getHealth());
    }

    @Test
    @DisplayName("Test if overridden equals and hashCode functions correctly compares two objects")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple().forClass(DummyUnit.class).verify();
    }
}
