package no.jakkar.wargames.model.units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Class for testing ranged units and their unique functionality.
 * 
 * @see InfantryUnit
 */
public class InfantryUnitTest {

    @Test
    @DisplayName("Test if object creation works with the full constructor.")
    void testFullConstructor() {
        InfantryUnit unit = new InfantryUnit("Unit", 100, 50, 25);

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(50, unit.getAttack());
        assertEquals(25, unit.getArmor());
    }

    @Test
    @DisplayName("Test if object creation works with the simple constructor.")
    void testSimpleConstructor() {
        InfantryUnit unit = new InfantryUnit("Unit", 100);

        // the attack and armor values should be retrieved from
        // somewhere global as they should be changeable
        // without breaking this test

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(15, unit.getAttack());
        assertEquals(10, unit.getArmor());
    }

    @Test
    @DisplayName("Test if description is not empty.")
    void testGetUnitDescription() {
        InfantryUnit unit = new InfantryUnit("Unit", 10);

        assertNotNull(unit.getUnitDescription());
        assert !unit.getUnitDescription().isEmpty();
    }
}
