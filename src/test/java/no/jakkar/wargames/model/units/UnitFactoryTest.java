package no.jakkar.wargames.model.units;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the unit factory and functinality.
 * 
 * @see UnitFactory
 */
public class UnitFactoryTest {

    static UnitFactory factoryInstance;

    @BeforeAll
    static void populateFactoryInstance() {
        factoryInstance = UnitFactory.getInstance();
    }

    @Test
    @DisplayName("Test if factory returns a populated list.")
    void testGetAvailableUnits() {
        String[] availableUnits = UnitFactory.getAvailableUnits();

        assert (availableUnits != null);
        assert (availableUnits.length > 0);
    }

    @Test
    @DisplayName("Test if a factory instance can be gotten and that they are equal.")
    void testGetInstance() {
        UnitFactory secondFactoryInstance = UnitFactory.getInstance();

        assert (factoryInstance != null);
        assert (factoryInstance.equals(secondFactoryInstance));

    }

    @Test
    @DisplayName("Test if it is possible to get a unit from the factory.")
    void testGetUnit() {
        Unit unit = factoryInstance.getUnit("RangedUnit", "Ranged", 10);

        assert (unit != null);
        assert (unit instanceof RangedUnit);
        assert (unit.getName() == "Ranged");
        assert (unit.getHealth() == 10);
    }

    @Test
    void testGetUnitWithMoreParameters() {
        Unit unit = factoryInstance.getUnit("RangedUnit", "Ranged", 10, 11, 12);

        assert (unit != null);
        assert (unit instanceof RangedUnit);
        assert (unit.getName() == "Ranged");
        assert (unit.getHealth() == 10);
        assert (unit.getAttack() == 11);
        assert (unit.getArmor() == 12);
    }

    @Test
    @DisplayName("Test requesting an unknown unit returns null.")
    void testGetUnknownUnit() {
        Unit unit = factoryInstance.getUnit("NotAValidUnit", "Ranged", 10);

        assert (unit == null);
    }

    @Test
    @DisplayName("Test if it is possible to get several units at once from the factory.")
    void testGetUnits() {
        List<Unit> units = factoryInstance.getUnits("RangedUnit", 5, "Ranged", 10);

        assert (units != null);
        assert (units.size() == 5);
        assert (units.get(0) instanceof RangedUnit);
        assert (units.get(0).getName() == "Ranged");
        assert (units.get(0).getHealth() == 10);
    }

    @Test
    void testGetUnitsWithMoreParameters() {
        List<Unit> units = factoryInstance.getUnits("RangedUnit", 5, "Ranged", 10, 11, 12);

        assert (units != null);
        assert (units.size() == 5);
        assert (units.get(0) instanceof RangedUnit);
        assert (units.get(0).getName() == "Ranged");
        assert (units.get(0).getHealth() == 10);
        assert (units.get(0).getAttack() == 11);
        assert (units.get(0).getArmor() == 12);
    }

    @Test
    @DisplayName("Test if requesting an unknown unit returns null.")
    void testGetUnknownUnits() {
        List<Unit> units = factoryInstance.getUnits("NotAValidUnit", 5, "Ranged", 10);

        assert (units == null);
    }

    @Test
    @DisplayName("Test if requesting a negative amount of units correctly throws an exception.")
    void testGetNegativeAmountOfUnits() {
        try {
            factoryInstance.getUnits("RangedUnit", -5, "Ranged", 10);
            assert false;
        } catch (IllegalArgumentException e) {
            assert true;
        }
    }

}
