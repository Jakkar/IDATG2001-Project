package no.jakkar.wargames.model.units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Class for testing cavalry units and their unique functionality.
 * 
 * @see CavalryUnit
 */
public class CavalryUnitTest {

    @Test
    @DisplayName("Test if object creation works with the full constructor.")
    void testFullConstructor() {
        CavalryUnit unit = new CavalryUnit("Unit", 100, 50, 25);

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(50, unit.getAttack());
        assertEquals(25, unit.getArmor());
    }

    @Test
    @DisplayName("Test if object creation works with the simple constructor.")
    void testSimpleConstructor() {
        CavalryUnit unit = new CavalryUnit("Unit", 100);

        // the attack and armor values should be retrieved from
        // somewhere global as they should be changeable
        // without breaking this test

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(20, unit.getAttack());
        assertEquals(12, unit.getArmor());
    }

    @Test
    @DisplayName("Test if attacks scale up properly.")
    void testGetAttackBonus() {
        CavalryUnit attacker = new CavalryUnit("Unit", 100);
        CavalryUnit defender = new CavalryUnit("Unit", 100);

        // make sure attack bonus is 6 before any attakcs
        assertEquals(6, attacker.getAttackBonus(null));

        // make sure attack bonus is 4 for conescutive attacks
        attacker.attack(defender, null);
        assertEquals(2, attacker.getAttackBonus(null));

        attacker.attack(defender, null);
        assertEquals(2, attacker.getAttackBonus(null));
    }

    @Test
    @DisplayName("Test if description is not empty.")
    void testGetUnitDescription() {
        CavalryUnit unit = new CavalryUnit("Unit", 10);

        assertNotNull(unit.getUnitDescription());
        assert !unit.getUnitDescription().isEmpty();
    }
}