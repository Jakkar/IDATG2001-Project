package no.jakkar.wargames.model.units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Class for testing ranged units and their unique functionality.
 * 
 * @see RangedUnit
 */
public class RangedUnitTest {

    @Test
    @DisplayName("Test if object creation works with the full constructor.")
    void testFullConstructor() {
        RangedUnit unit = new RangedUnit("Unit", 100, 50, 25);

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(50, unit.getAttack());
        assertEquals(25, unit.getArmor());
    }

    @Test
    @DisplayName("Test if object creation works with the simple constructor.")
    void testSimpleConstructor() {
        RangedUnit unit = new RangedUnit("Unit", 100);

        // the attack and armor values should be retrieved from
        // somewhere global as they should be changeable
        // without breaking this test

        assertEquals("Unit", unit.getName());
        assertEquals(100, unit.getHealth());
        assertEquals(15, unit.getAttack());
        assertEquals(8, unit.getArmor());
    }

    @Test
    @DisplayName("Test if defence scales down properly.")
    void testGetResistBonus() {
        RangedUnit attacker = new RangedUnit("Unit", 100);
        RangedUnit defender = new RangedUnit("Unit", 100);

        // make sure first defence bonus is 6
        assertEquals(6, defender.getResistBonus(null));

        // make sure defence bonus after 1 attack is 4
        attacker.attack(defender, null);
        assertEquals(4, defender.getResistBonus(null));

        // make sure defence bonus for consecutive attacks is 2
        attacker.attack(defender, null);
        assertEquals(2, defender.getResistBonus(null));

        attacker.attack(defender, null);
        assertEquals(2, defender.getResistBonus(null));
    }

    @Test
    @DisplayName("Test if description is not empty.")
    void testGetUnitDescription() {
        RangedUnit unit = new RangedUnit("Unit", 10);

        assertNotNull(unit.getUnitDescription());
        assert !unit.getUnitDescription().isEmpty();
    }
}
