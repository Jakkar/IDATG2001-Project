package no.jakkar.wargames.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.units.UnitFactory;

/**
 * Class for testing functionality of writing and reading to CSV files.
 * 
 * @see CSVFileHandler
 */
public class CSVFileHandlerTest {
    private Army testArmy = new Army("Test Army");

    @BeforeEach
    void populateTestArmy() {
        testArmy.add(UnitFactory.getInstance().getUnit("CommanderUnit", "Commander", 15));
        testArmy.add(UnitFactory.getInstance().getUnit("CavalryUnit", "Cavalry", 10));
        testArmy.add(UnitFactory.getInstance().getUnit("RangedUnit", "Ranged", 5));
    }

    @Test
    @DisplayName("Test if writing army to file works.")
    void testWriteArmyToCSVFile(@TempDir Path tempDir) {
        File file = tempDir.resolve("output.csv").toFile();
        try {
            // write to file
            CSVFileHandler.writeArmyToCSVFile(file, testArmy);
            assert file.exists();

            // check that written army still is the same
            Army readArmy = CSVFileHandler.readArmyFromCSVFile(file);
            assertEquals(testArmy, readArmy);
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    @DisplayName("Test if reading army from file works.")
    void testReadArmyFromCSVFile() throws URISyntaxException {
        File file = new File(this.getClass().getResource("testArmy.csv").toURI());

        try {
            // read from test file and check it's the same as test army
            Army readArmy = CSVFileHandler.readArmyFromCSVFile(file);
            assertEquals(testArmy, readArmy);
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    @DisplayName("Test if reading from non-existing file fails properly.")
    void testReadArmyFromNotExistingFile() {
        File notFile = Path.of("thisfiledoesnotexist:O.file").toFile();
        try {
            // read from test file and make sure exception is thrown
            CSVFileHandler.readArmyFromCSVFile(notFile);
            assert false;
        } catch (FileNotFoundException e) {
            // correct exception thrown, as IOException or CsvValidationException
            // should not be thrown
            assert true;
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    @DisplayName("Test if reading from empty file fails properly.")
    void testReadArmyFromEmptyFile() throws URISyntaxException {
        File notCsvFile = new File(this.getClass().getResource("empty.txt").toURI());

        try {
            // read from test file and make sure exception is thrown
            CSVFileHandler.readArmyFromCSVFile(notCsvFile);
            assert false;
        } catch (IllegalArgumentException e) {
            // correct exception thrown, as IOException, FileNotFoundException or
            // CsvValidationException should not be thrown
            assert true;
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }
}
