package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

/**
 * Subclass of Unit to represent a unit with ranged attacks.
 */
public class RangedUnit extends Unit {

    protected RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    @Override
    public RangedUnit makeCopy() {
        return new RangedUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public Terrain getFavourableTerrain() {
        return Terrain.HILLS;
    }

    @Override
    public Terrain getUnfavourableTerrain() {
        return Terrain.FOREST;
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        int attack_bonus = 3;

        if (terrain == getFavourableTerrain()) {
            attack_bonus += 4;
        }

        if (terrain == getUnfavourableTerrain()) {
            attack_bonus -= 1;
        }

        return attack_bonus;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        switch (this.getTimesAttacked()) {
            case 0:
                return 6;
            case 1:
                return 4;
            default:
                return 2;
        }
    }

    @Override
    public String getUnitDescription() {
        StringBuilder str = new StringBuilder();

        return str
                .append("RangedUnit is a unit which excels at a distance.")
                .append(System.lineSeparator())
                .append("It's default values are 15 attack and 8 armor.")
                .append(System.lineSeparator())
                .append("It favors ").append(getFavourableTerrain()).append(".")
                .append(System.lineSeparator())
                .append("It is weaker in ").append(getUnfavourableTerrain()).append(".")
                .toString();
    }
}
