package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

/**
 * Subclass of Unit to represent a basic unit with not specialities.
 */
public class InfantryUnit extends Unit {

    protected InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    @Override
    public InfantryUnit makeCopy() {
        return new InfantryUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public Terrain getFavourableTerrain() {
        return Terrain.FOREST;
    }

    @Override
    public Terrain getUnfavourableTerrain() {
        return null;
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        int attack_bonus = 0;

        if (terrain == getFavourableTerrain()) {
            attack_bonus += 4;
        }

        return attack_bonus;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        int resist_bonus = 1;

        if (terrain == getFavourableTerrain()) {
            resist_bonus += 4;
        }

        return resist_bonus;
    }

    @Override
    public String getUnitDescription() {
        StringBuilder str = new StringBuilder();

        return str
                .append("InfantryUnit is a basic unit with average stats.")
                .append(System.lineSeparator())
                .append("It's default values are 15 attack and 10 armor.")
                .append(System.lineSeparator())
                .append("It favors ").append(getFavourableTerrain()).append(".")
                .toString();
    }
}
