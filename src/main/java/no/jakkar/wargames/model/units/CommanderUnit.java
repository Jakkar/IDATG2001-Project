package no.jakkar.wargames.model.units;

/**
 * Subclass of CavalryUnit to represent a similar unit with different stats.
 */
public class CommanderUnit extends CavalryUnit {

    protected CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    @Override
    public CommanderUnit makeCopy() {
        return new CommanderUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public String getUnitDescription() {
        StringBuilder str = new StringBuilder();

        return str
                .append("CommanderUnit is similar to a CavalryUnit, but with even stronger stats.")
                .append(System.lineSeparator())
                .append("It's default values are 25 attack and 15 armor.")
                .append(System.lineSeparator())
                .append("It favors ").append(getFavourableTerrain()).append(".")
                .append(System.lineSeparator())
                .append("It is weaker in ").append(getUnfavourableTerrain()).append(".")
                .toString();
    }
}
