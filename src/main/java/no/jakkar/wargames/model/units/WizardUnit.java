package no.jakkar.wargames.model.units;

public abstract class WizardUnit extends Unit {

    protected WizardUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected WizardUnit(String name, int health) {
        super(name, health, 15, 8);
    }
}
