package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

/**
 * Abstract class representing a unit with it's values and attack logic.
 */
public abstract class Unit {

    /**
     * Name of the unit.
     */
    private String name;

    /**
     * Health of the unit.
     */
    private int health;

    /**
     * Attack value of the unit.
     */
    private int attack;

    /**
     * Armor value of the unit.
     */
    private int armor;

    /**
     * Counter for how many times unit has been attacked.
     */
    private int timesAttacked;

    /**
     * Counter for how many times unit has defended while attacking.
     */
    private int timesDefended;

    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        setHealth(health);
        this.attack = attack;
        this.armor = armor;
        this.timesAttacked = 0;
        this.timesDefended = 0;
    }

    /**
     * Method for getting a deep copy of a unit.
     * 
     * @return copy of unit
     */
    public abstract Unit makeCopy();

    /**
     * Method for performing an attack on a unit.
     * 
     * @param opponent unit recieving the attack
     * @param terrain  terrain attack is happening in
     */
    public void attack(Unit opponent, Terrain terrain) {
        opponent.setHealth(opponent.health -
                (this.attack + this.getAttackBonus(terrain)) +
                (opponent.armor + opponent.getResistBonus(terrain)));
        opponent.registerAttack();
        this.registerDefence();
    }

    /**
     * Returns the current attack bonus of the unit.
     * 
     * @param terrain terrain to get the bonus for
     * 
     * @return current attack bonus
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * Returns the current resistance bonus of the unit.
     * 
     * @param terrain terrain to get the bonus for
     * 
     * @return current resistance bonus
     */
    public abstract int getResistBonus(Terrain terrain);

    /**
     * Returns the units favorable terrain.
     * 
     * @return the favorable terrain or null if no special terrain
     */
    public abstract Terrain getFavourableTerrain();

    /**
     * Returns the units unfavorable terrain.
     * 
     * @return the unfavourable terrain or null if no special terrain
     */
    public abstract Terrain getUnfavourableTerrain();

    /**
     * Returns a description of the unit and it's specialties.
     * 
     * @return the description of the unit
     */
    public abstract String getUnitDescription();

    /**
     * Returns the current name of the unit.
     * 
     * @return the name of the unit
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name of the unit.
     * 
     * @param name the name of the unit
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the current health of the unit.
     * 
     * @return current health
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * Sets the health of the unit.
     * Negative values are set to 0!
     * 
     * @param health new health value
     */
    public void setHealth(int health) {
        if (health < 0) {
            this.health = 0;
        } else {
            this.health = health;
        }
    }

    /**
     * Returns the current attack value of the unit-
     * 
     * @return current attack value
     */
    public int getAttack() {
        return this.attack;
    }

    /**
     * Sets the attack value of the unit.
     * 
     * @param attack new attack value
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Returns the current armor value of the unit.
     * 
     * @return current armor value
     */
    public int getArmor() {
        return this.armor;
    }

    /**
     * Sets the armor value of the unit.
     * 
     * @param armor new armor value
     */
    public void setArmor(int armor) {
        this.armor = armor;
    }

    /**
     * Method for registering that a unit has been attacked.
     */
    private void registerAttack() {
        timesAttacked++;
    }

    /**
     * Method for registering that a unit has defended while attacking.
     */
    private void registerDefence() {
        timesDefended++;
    }

    /**
     * Method for getting the amount of times a unit has attacked.
     * 
     * @return amount of times unit has attacked
     */
    public int getTimesAttacked() {
        return timesAttacked;
    }

    /**
     * Method for getting the amount of times a unit has defended.
     * 
     * @return amount of times unit has defended
     */
    public int getTimesDefended() {
        return timesDefended;
    }

    ////////////////////////////////
    // toString, hashcode, equals //
    ////////////////////////////////

    @Override
    public String toString() {
        return String.format("Unit [armor=%s, attack=%s, health=%s, name=%s]",
                armor,
                attack,
                health,
                name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + armor;
        result = prime * result + attack;
        result = prime * result + health;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + timesAttacked;
        result = prime * result + timesDefended;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Unit other = (Unit) obj;
        if (armor != other.armor)
            return false;
        if (attack != other.attack)
            return false;
        if (health != other.health)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (timesAttacked != other.timesAttacked)
            return false;
        if (timesDefended != other.timesDefended)
            return false;
        return true;
    }

}
