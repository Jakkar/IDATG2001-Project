package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

/**
 * Subclass of Unit to represent a unit with a stronger defence.
 */
public class CavalryUnit extends Unit {

    protected CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    @Override
    public CavalryUnit makeCopy() {
        return new CavalryUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public Terrain getFavourableTerrain() {
        return Terrain.PLAINS;
    }

    @Override
    public Terrain getUnfavourableTerrain() {
        return Terrain.FOREST;
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        int attack_bonus = 2;

        // if this is the first time attacking
        if (this.getTimesDefended() == 0) {
            attack_bonus += 4;
        }

        if (terrain == getFavourableTerrain()) {
            attack_bonus += 4;
        }

        return attack_bonus;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        int resist_bonus = 1;

        if (terrain == getUnfavourableTerrain()) {
            resist_bonus = 0;
        }

        return resist_bonus;
    }

    @Override
    public String getUnitDescription() {
        StringBuilder str = new StringBuilder();

        return str
                .append("CavalryUnit is a unit with strong defence.")
                .append(System.lineSeparator())
                .append("It's default values are 20 attack and 12 armor.")
                .append(System.lineSeparator())
                .append("It favors ").append(getFavourableTerrain()).append(".")
                .append(System.lineSeparator())
                .append("It is weaker in ").append(getUnfavourableTerrain()).append(".")
                .toString();
    }
}
