package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

public class MountainWizardUnit extends WizardUnit {

    protected MountainWizardUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    protected MountainWizardUnit(String name, int health) {
        super(name, health);
    }

    @Override
    public MountainWizardUnit makeCopy() {
        return new MountainWizardUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public Terrain getFavourableTerrain() {
        return Terrain.HILLS;
    }

    @Override
    public Terrain getUnfavourableTerrain() {
        return null;
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        int attack_bonus = 2;

        if (terrain == getFavourableTerrain()) {
            attack_bonus += 10;
        }

        return attack_bonus;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        int resist_bonus = 0;

        return resist_bonus;
    }

    @Override
    public String getUnitDescription() {
        StringBuilder str = new StringBuilder();

        return str
                .append("Wizards are a class of units that are can't take much of a beating, ")
                .append("but deliver a proper punch in their respective environments!")
                .append(System.lineSeparator())
                .append("It's default values are 15 attack and 8 armor.")
                .append(System.lineSeparator())
                .append("This one gets a big attack boost in ").append(getFavourableTerrain()).append(".")
                .toString();
    }
}
