package no.jakkar.wargames.model.units;

import no.jakkar.wargames.model.battle.Terrain;

/**
 * Subclass of unit with no values or abilities.
 * 
 * Used for testing abstract class methods and as temporary unit in GUI.
 */
public class DummyUnit extends Unit {

    public DummyUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public DummyUnit(String name, int health) {
        super(name, health, 0, 0);
    }

    @Override
    public DummyUnit makeCopy() {
        return new DummyUnit(this.getName(), this.getHealth(), this.getAttack(), this.getArmor());
    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        return 0;
    }

    @Override
    public Terrain getFavourableTerrain() {
        return null;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        return 0;
    }

    @Override
    public Terrain getUnfavourableTerrain() {
        return null;
    }

    @Override
    public String getUnitDescription() {
        return "";
    }

}
