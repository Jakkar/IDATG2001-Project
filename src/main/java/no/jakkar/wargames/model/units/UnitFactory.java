package no.jakkar.wargames.model.units;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class for getting Unit instances without knowing exact class.
 */
public class UnitFactory {

    /**
     * Singleton instance of factory.
     */
    private static UnitFactory unitFactory_instance;

    /**
     * List of possible units to instantiate.
     */
    private static final String[] availableUnits = {
            "CavalryUnit",
            "CommanderUnit",
            "InfantryUnit",
            "RangedUnit",
            "ForestWizardUnit",
            "PlantWizardUnit",
            "MountainWizardUnit"
    };

    private UnitFactory() {

    }

    /**
     * Lazy method for getting the factory instance.
     * 
     * @return UnitFactory instance
     */
    public static UnitFactory getInstance() {
        if (unitFactory_instance == null) {
            unitFactory_instance = new UnitFactory();
        }

        return unitFactory_instance;
    }

    /**
     * Get a list of all available units in factory.
     * 
     * @return String array of available units
     */
    public static String[] getAvailableUnits() {
        return availableUnits;
    }

    /**
     * Method for getting a single unit.
     * 
     * @param unit   type of unit (CavalryUnit, CommanderUnit etc.)
     * @param name   name of unit
     * @param health health points of unit
     * 
     * @return null if unit can not be intantiated or Unit object if successful
     */
    public Unit getUnit(String unit, String name, int health) {
        // check if requested unit is available in factory
        if (!Arrays.asList(availableUnits).contains(unit)) {
            return null;
        }

        // return requested unit
        switch (unit) {
            case "CavalryUnit":
                return new CavalryUnit(name, health);
            case "CommanderUnit":
                return new CommanderUnit(name, health);
            case "InfantryUnit":
                return new InfantryUnit(name, health);
            case "RangedUnit":
                return new RangedUnit(name, health);
            case "ForestWizardUnit":
                return new ForestWizardUnit(name, health);
            case "PlantWizardUnit":
                return new PlantWizardUnit(name, health);
            case "MountainWizardUnit":
                return new MountainWizardUnit(name, health);
            default:
                return null;
        }
    }

    /**
     * Method for getting a single unit.
     * 
     * @param unit   type of unit (CavalryUnit, CommanderUnit etc.)
     * @param name   name of unit
     * @param health health points of unit
     * @param attack attack value of unit
     * @param armor  armor value of unit
     * 
     * @return null if unit can not be intantiated or Unit object if successful
     */
    public Unit getUnit(String unit, String name, int health, int attack, int armor) {
        Unit newUnit = getUnit(unit, name, health);

        if (newUnit == null) {
            return null;
        }

        newUnit.setAttack(attack);
        newUnit.setArmor(armor);

        return newUnit;
    }

    /**
     * Method for getting a list of identical units.
     * 
     * @param unit   type of unit (CavalryUnit, CommanderUnit etc.)
     * @param amount amount of units to instatiate
     * @param name   name of units
     * @param health health points of units
     * 
     * @return null if unit can not be instatiated or list of units if successful
     * 
     * @throws IllegalArgumentException if amount is {@literal <} 1, as at least one
     *                                  unit must be instatiated
     */
    public List<Unit> getUnits(String unit, int amount, String name, int health) throws IllegalArgumentException {
        // check if requested unit is available in factory
        if (!Arrays.asList(availableUnits).contains(unit)) {
            return null;
        }

        // must create at least 1 unit
        if (amount < 1) {
            throw new IllegalArgumentException("Must create at least 1 unit");
        }

        // create a list with n amount of units
        return Collections.nCopies(amount, getUnit(unit, name, health));
    }

    /**
     * Method for getting a list of identical units.
     * 
     * @param unit   type of unit (CavalryUnit, CommanderUnit etc.)
     * @param amount amount of units to instatiate
     * @param name   name of units
     * @param health health points of units
     * @param attack attack value of unit
     * @param armor  armor value of unit
     * 
     * @return null if unit can not be instatiated or list of units if successful
     * 
     * @throws IllegalArgumentException if amount is {@literal <} 1, as at least one
     *                                  unit must be instatiated
     */
    public List<Unit> getUnits(String unit,
            int amount,
            String name,
            int health,
            int attack,
            int armor) throws IllegalArgumentException {
        Unit newUnit = getUnit(unit, name, health);

        if (newUnit == null) {
            return null;
        }

        newUnit.setAttack(attack);
        newUnit.setArmor(armor);

        return Collections.nCopies(amount, newUnit);
    }
}
