package no.jakkar.wargames.model.army;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import no.jakkar.wargames.model.units.CavalryUnit;
import no.jakkar.wargames.model.units.CommanderUnit;
import no.jakkar.wargames.model.units.InfantryUnit;
import no.jakkar.wargames.model.units.RangedUnit;
import no.jakkar.wargames.model.units.Unit;

/**
 * Class representing an army of units.
 */
public class Army {
    /**
     * Name of the army.
     */
    private String name;

    /**
     * A list of units in the army.
     */
    private List<Unit> units;

    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }

    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * Method for creating a deep copy of an army.
     * 
     * @return copy of army
     */
    public Army makeCopy() {
        return new Army(
                this.getName(),
                this.getAllUnits().stream().map(unit -> unit.makeCopy()).collect(Collectors.toList()));
    }

    /**
     * Method for adding a single unit to the army.
     * 
     * @param unit a unit to be added to the army
     */
    public void add(Unit unit) {
        this.units.add(unit);
    }

    /**
     * Method for adding a list of units to the army.
     * 
     * @param unit a list of units to be added to the army
     */
    public void addAll(List<Unit> unit) {
        this.units.addAll(unit);
    }

    /**
     * Method for removing a unit from the army.
     * 
     * @param unit unit to remove from the army
     */
    public void remove(Unit unit) {
        units.remove(unit);
    }

    /**
     * Method for checking if the army has any units.
     * 
     * @return true if the army has units, false if the army has no units
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Returns the list of all units in the army.
     * 
     * @return the list of all units
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Returns a random unit from the army.
     * 
     * @return the random unit
     */
    public Unit getRandom() {
        Random rand = new Random();
        return units.get(rand.nextInt(units.size()));
    }

    /**
     * Returns the name of the army.
     * 
     * @return the name of the army
     */
    public String getName() {
        return name;
    }

    /**
     * Returns a list of all the infantry units in the army.
     * 
     * @return a list of all the infantry units in the army
     */
    public List<Unit> getInfantryUnits() {
        return getAllUnits()
                .stream()
                .filter(c -> c instanceof InfantryUnit)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of all the cavalry units in the army.
     * 
     * @return a list of all the cavalry units in the army
     */
    public List<Unit> getCavalryUnits() {
        return getAllUnits()
                .stream()
                .filter(c -> c instanceof CavalryUnit)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of all the commander units in the army.
     * 
     * @return a list of all the commander units in the army
     */
    public List<Unit> getCommanderUnits() {
        return getAllUnits()
                .stream()
                .filter(c -> c instanceof CommanderUnit)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of all the ranged units in the army.
     * 
     * @return a list of all the ranged units in the army
     */
    public List<Unit> getRangedUnits() {
        return getAllUnits()
                .stream()
                .filter(c -> c instanceof RangedUnit)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of all the units of a specific class in the army.
     * 
     * @param unit the class of the units to retrieve
     * 
     * @return a list of all the units of the provided class
     */
    public List<Unit> getUnitsByType(Class<? extends Unit> unit) {
        return getAllUnits()
                .stream()
                .filter(c -> c.getClass().equals(unit))
                .collect(Collectors.toList());
    }

    ////////////////////////////////
    // toString, hashcode, equals //
    ////////////////////////////////

    @Override
    public String toString() {
        return String.format("%s (%s units)", name, units.size());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((units == null) ? 0 : units.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Army other = (Army) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (units == null) {
            if (other.units != null)
                return false;
        } else if (!units.equals(other.units))
            return false;
        return true;
    }
}
