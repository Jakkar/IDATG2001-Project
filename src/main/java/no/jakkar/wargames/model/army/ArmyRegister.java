package no.jakkar.wargames.model.army;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import no.jakkar.wargames.model.CSVFileHandler;

/**
 * Class for storing armies saved "in" program.
 */
public class ArmyRegister {
    /**
     * Singleton instance of register.
     */
    private static ArmyRegister armyRegisterInstance;

    /**
     * List of armies.
     */
    private static ArrayList<Army> armyList;

    private ArmyRegister() {

    }

    /**
     * Lazy method for getting instance.
     * Will automatically load armies on first retrieval.
     * 
     * @return ArmyRegister instance
     */
    public static ArmyRegister getInstance() {
        if (armyRegisterInstance == null) {
            armyRegisterInstance = new ArmyRegister();
            armyRegisterInstance.loadArmiesFromDefaultLocation();
        }

        return armyRegisterInstance;
    }

    /**
     * Method for updating registry from army files.
     */
    public void loadArmiesFromDefaultLocation() {
        // reset the list
        armyList = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get("./armystorage/"))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path -> {
                        try {
                            Army army = CSVFileHandler.readArmyFromCSVFile(path.toFile());
                            armyList.add(army);
                        } catch (Exception e) {
                            // just skip if army is not possible to load
                        }
                    });
        } catch (IOException e) {
            // maybe do something in the future? but for now just load what is possible
            // and move on

            // if nothing is possible then the armylist will just be empty and new armies
            // will have to be created
        }
    }

    /**
     * Returns copy of armylist to use wherever.
     * Assures armies modified elsewhere also won't be modified here.
     * 
     * @return list of store armies
     */
    public List<Army> getArmies() {
        return armyList.stream().map(army -> army.makeCopy()).collect(Collectors.toList());
    }
}
