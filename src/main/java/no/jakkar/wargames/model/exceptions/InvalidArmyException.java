package no.jakkar.wargames.model.exceptions;

/**
 * Class for representing an exception about invalid army data.
 */
public class InvalidArmyException extends Exception {
    public InvalidArmyException(String errorMessage) {
        super(errorMessage);
    }
}
