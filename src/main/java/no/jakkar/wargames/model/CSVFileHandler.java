package no.jakkar.wargames.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.exceptions.InvalidArmyException;
import no.jakkar.wargames.model.units.Unit;
import no.jakkar.wargames.model.units.UnitFactory;

/**
 * Class for holding static methods related to read/writing armies to files.
 */
public class CSVFileHandler {

    /**
     * Method for writing an army to a file.
     * 
     * @param file file to write army to
     * @param army army to write
     * 
     * @throws IOException unable to write to file
     */
    public static void writeArmyToCSVFile(File file, Army army) throws IOException {
        File csvFile = file;
        csvFile.getParentFile().mkdirs();

        if (!csvFile.getName().endsWith(".csv") && !csvFile.getName().endsWith(".CSV")) {
            csvFile = new File(file.getPath() + ".csv");
        }

        FileWriter fw = new FileWriter(csvFile);
        CSVWriter writer = new CSVWriter(fw);

        // write name of army on first line
        writer.writeNext(new String[] { army.getName() });

        // write units one by one, line by line
        for (Unit unit : army.getAllUnits()) {
            writer.writeNext(new String[] {
                    unit.getClass().getSimpleName(),
                    unit.getName(),
                    String.valueOf(unit.getHealth()),
                    String.valueOf(unit.getAttack()),
                    String.valueOf(unit.getArmor())
            });
        }

        writer.close();
        fw.close();
    }

    /**
     * Method for reading and creating an army from a file.
     * 
     * @param file file to read from
     * 
     * @return the imported army
     * 
     * @throws FileNotFoundException    unable to find file
     * @throws IllegalArgumentException file is empty
     * @throws CsvValidationException   csv data is malformed
     * @throws IOException              unable to read from file
     */
    public static Army readArmyFromCSVFile(File file)
            throws FileNotFoundException,
            IllegalArgumentException,
            InvalidArmyException,
            CsvValidationException,
            IOException {
        FileReader fr = new FileReader(file);
        CSVReader reader = new CSVReader(fr);

        // first line should be army name
        String[] armyName = reader.readNext();

        // if armyName is null, there isn't even a newline character there,
        // so assume empty file
        if (armyName == null) {
            reader.close();
            fr.close();
            throw new IllegalArgumentException("File is empty");
        }

        // set army name
        Army army = new Army(armyName[0]);

        // read units and add one by one
        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            try {
                Unit unit = UnitFactory.getInstance().getUnit(
                        nextLine[0], // unit type
                        nextLine[1], // unit name
                        Integer.parseInt(nextLine[2]), // unit health stat
                        Integer.parseInt(nextLine[3]), // unit attack stat
                        Integer.parseInt(nextLine[4])); // unit armor stat

                // skip if invalid unit
                if (unit == null) {
                    continue;
                }

                army.add(unit);
            } catch (Exception e) {
                reader.close();
                fr.close();
                throw new InvalidArmyException("Unable to parse army units!");
            }
        }

        reader.close();
        fr.close();
        return army;
    }
}
