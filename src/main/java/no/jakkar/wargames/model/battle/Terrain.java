package no.jakkar.wargames.model.battle;

/**
 * Enum for keeping track of possible terrains to battle in.
 */
public enum Terrain {
    HILLS, PLAINS, FOREST;
}
