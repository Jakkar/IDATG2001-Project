package no.jakkar.wargames.model.battle;

import java.util.ArrayList;
import java.util.List;

import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.units.Unit;

/**
 * Class representing a battle between two armies.
 * Can simulate a battle and determine a winner.
 */
public class Battle {
    /**
     * The first army in the war.
     */
    private Army armyOne;

    /**
     * The original first army in the war.
     */
    private final Army originalArmyOne;

    /**
     * The second army in the war.
     */
    private Army armyTwo;

    /**
     * The original second army in the war
     */
    private final Army originalArmyTwo;

    /**
     *  
     */
    private Terrain terrain;

    /**
     * List to keep history of battle states.
     */
    private List<BattleState> battleStates;

    /**
     * Army that won after simulation.
     */
    private Army victoriousArmy;

    /**
     * Bool for keeping track of if battle has been simulated already or not.
     */
    private boolean battleSimulated;

    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        this.battleSimulated = false;

        this.armyOne = armyOne;
        this.originalArmyOne = armyOne.makeCopy();

        this.armyTwo = armyTwo;
        this.originalArmyTwo = armyTwo.makeCopy();

        this.terrain = terrain;

        this.battleStates = new ArrayList<>();

        this.victoriousArmy = null;
    }

    /**
     * Method for simulating a battle between armies.
     * Alternates attacks between armies.
     * Picks a random troop from army one and army two and makes them attack each
     * other for each simulation step.
     */
    public void simulate() {

        if (battleSimulated) {
            throw new IllegalStateException("Battle has already been simulated!");
        }

        // value to easily alternate attacks from armies
        boolean toggle = false;

        // simulation loop
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {

            if (toggle) {
                Unit attacker = armyOne.getRandom();
                Unit opponent = armyTwo.getRandom();
                attacker.attack(opponent, terrain);

                if (opponent.getHealth() <= 0) {
                    armyTwo.remove(opponent);
                }
            } else {
                Unit attacker = armyTwo.getRandom();
                Unit opponent = armyOne.getRandom();
                attacker.attack(opponent, terrain);

                if (opponent.getHealth() <= 0) {
                    armyOne.remove(opponent);
                }
            }

            battleStates.add(new BattleState(armyOne, armyTwo, terrain));

            toggle = !toggle;
        }

        battleSimulated = true;

        if (armyOne.hasUnits()) {
            this.victoriousArmy = armyOne;
        } else {
            this.victoriousArmy = armyTwo;
        }
    }

    /**
     * Method for resetting battle to original armies and clearing states.
     */
    public void reset() {
        this.armyOne = originalArmyOne.makeCopy();
        this.armyTwo = originalArmyTwo.makeCopy();
        this.battleStates.clear();
        this.battleSimulated = false;
        this.victoriousArmy = null;
    }

    /**
     * Method for getting the first of the armies in the battle.
     * 
     * @return the first army in the battle
     */
    public Army getArmyOne() {
        return armyOne;
    }

    /**
     * Method for getting the second army in the battle.
     * 
     * @return the second army in the battle
     */
    public Army getArmyTwo() {
        return armyTwo;
    }

    /**
     * Method for checking if the battle already has been simulated.
     * 
     * @return simulation status for battle
     */
    public boolean isBattleSimulated() {
        return battleSimulated;
    }

    /**
     * Method for getting the battle states of the battle.
     * Empty if simulation has not been ran yet.
     * 
     * @return list of states in the battle
     */
    public List<BattleState> getBattleStates() {
        if (!battleSimulated) {
            throw new IllegalStateException("Battle has not yet been simulated!");
        }

        return battleStates;
    }

    /**
     * Method for getting the victorious army after simulation.
     * 
     * @return victorious army
     */
    public Army getVictoriousArmy() {
        if (!battleSimulated) {
            throw new IllegalStateException("Battle has not yet been simulated!");
        }

        return this.victoriousArmy;
    }

    ////////////////////////////////
    // toString, hashcode, equals //
    ////////////////////////////////

    @Override
    public String toString() {
        return String.format("Battle [armyOne=%s, armyTwo=%s]", armyOne, armyTwo);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((armyOne == null) ? 0 : armyOne.hashCode());
        result = prime * result + ((armyTwo == null) ? 0 : armyTwo.hashCode());
        result = prime * result + (battleSimulated ? 1231 : 1237);
        result = prime * result + ((battleStates == null) ? 0 : battleStates.hashCode());
        result = prime * result + ((originalArmyOne == null) ? 0 : originalArmyOne.hashCode());
        result = prime * result + ((originalArmyTwo == null) ? 0 : originalArmyTwo.hashCode());
        result = prime * result + ((terrain == null) ? 0 : terrain.hashCode());
        result = prime * result + ((victoriousArmy == null) ? 0 : victoriousArmy.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Battle other = (Battle) obj;
        if (armyOne == null) {
            if (other.armyOne != null)
                return false;
        } else if (!armyOne.equals(other.armyOne))
            return false;
        if (armyTwo == null) {
            if (other.armyTwo != null)
                return false;
        } else if (!armyTwo.equals(other.armyTwo))
            return false;
        if (battleSimulated != other.battleSimulated)
            return false;
        if (battleStates == null) {
            if (other.battleStates != null)
                return false;
        } else if (!battleStates.equals(other.battleStates))
            return false;
        if (originalArmyOne == null) {
            if (other.originalArmyOne != null)
                return false;
        } else if (!originalArmyOne.equals(other.originalArmyOne))
            return false;
        if (originalArmyTwo == null) {
            if (other.originalArmyTwo != null)
                return false;
        } else if (!originalArmyTwo.equals(other.originalArmyTwo))
            return false;
        if (terrain != other.terrain)
            return false;
        if (victoriousArmy == null) {
            if (other.victoriousArmy != null)
                return false;
        } else if (!victoriousArmy.equals(other.victoriousArmy))
            return false;
        return true;
    }

}
