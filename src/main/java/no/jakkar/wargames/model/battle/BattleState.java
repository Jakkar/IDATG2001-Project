package no.jakkar.wargames.model.battle;

import no.jakkar.wargames.model.army.Army;

/**
 * Class representing a state in a battle.
 * Contains copies of armies and terrain for moment.
 */
public class BattleState {

    /**
     * The first army in the war.
     */
    private final Army armyOne;

    /**
     * The second army in the war.
     */
    private final Army armyTwo;

    /**
     * The terrain of the battle.
     */
    private final Terrain terrain;

    public BattleState(Army armyOne, Army armyTwo, Terrain terrain) {
        this.armyOne = armyOne.makeCopy();
        this.armyTwo = armyTwo.makeCopy();
        this.terrain = terrain;
    }

    /**
     * Returns the first of the stored armies.
     * 
     * @return the first army
     */
    public Army getArmyOne() {
        return armyOne;
    }

    /**
     * Returns the second of the stored armies.
     * 
     * @return the second army
     */
    public Army getArmyTwo() {
        return armyTwo;
    }

    /**
     * Returns the stored terrain.
     * 
     * @return the stored terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }

}
