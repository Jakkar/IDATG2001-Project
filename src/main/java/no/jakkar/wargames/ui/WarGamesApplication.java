package no.jakkar.wargames.ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Class representing entry point of JavaFX application.
 */
public class WarGamesApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setResizable(false);

        SceneManager.getInstance().init(primaryStage);
        SceneManager.getInstance().displayScene(ApplicationScene.START);

        primaryStage.show();
    }

}
