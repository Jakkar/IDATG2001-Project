package no.jakkar.wargames.ui.utils;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import no.jakkar.wargames.model.units.DummyUnit;
import no.jakkar.wargames.model.units.UnitFactory;

/**
 * Class for use in CreateNewArmyController.
 * 
 * Has observable values for simple use with JavaFX tables and is modeled for
 * the usecase in the "Create New War" page.
 */
public class UnitsTableEntry {
    /**
     * Unit for storing various unit values
     */
    private DummyUnit unit;

    /**
     * Observable string for unit name.
     */
    private SimpleStringProperty unitName;

    /**
     * Observable string for unit type.
     */
    private SimpleStringProperty unitType;

    /**
     * Observable boolean for if unit should have custom stats or not.
     */
    private SimpleBooleanProperty useCustomStats;

    /**
     * Observable integer for amount of units.
     */
    private SimpleIntegerProperty amountOfUnits;

    public UnitsTableEntry() {
        this.unit = new DummyUnit("Unit", 15, 10, 5);
        this.unitName = new SimpleStringProperty("Unit");
        this.unitType = new SimpleStringProperty(UnitFactory.getAvailableUnits()[0]);
        this.useCustomStats = new SimpleBooleanProperty(false);
        this.amountOfUnits = new SimpleIntegerProperty(1);
    }

    /**
     * Returns the current unit in this entry.
     * 
     * @return unit in this entry
     */
    public DummyUnit getUnit() {
        return unit;
    }

    /**
     * Returns observable name of the unit in this entry.
     * 
     * @return observable name of unit in this entry
     */
    public SimpleStringProperty getUnitName() {
        return unitName;
    }

    /**
     * Sets the name of the unit in this entry.
     * 
     * @param unitName name of the unit
     */
    public void setUnitName(String unitName) {
        this.unitName.set(unitName);
    }

    /**
     * Returns observable value of the type this unit is going to be.
     * 
     * @return observable value of the type of unit
     */
    public SimpleStringProperty getUnitType() {
        return unitType;
    }

    /**
     * Sets the type of unit this is going to be.
     * 
     * @param unitType type of unit
     */
    public void setUnitType(String unitType) {
        this.unitType.set(unitType);
    }

    /**
     * Returns observable value of if this unit should make use of custom stats.
     * 
     * @return observable value of custom stats status
     */
    public SimpleBooleanProperty getUseCustomStats() {
        return useCustomStats;
    }

    /**
     * Sets the value of if this unit should make use of custom stats.
     * 
     * @param useCustomStats if the unit should make use of custom stats
     */
    public void setUseCustomStats(boolean useCustomStats) {
        this.useCustomStats.set(useCustomStats);
    }

    /**
     * Gets an observable value of how many copies of this unit there should be.
     * 
     * @return observable value of amount of unit copies.
     */
    public SimpleIntegerProperty getAmountOfUnits() {
        return amountOfUnits;
    }

    /**
     * Sets the amount of copies of this unit.
     * 
     * @param amountOfUnits amount of copies of this unit
     */
    public void setAmountOfUnits(int amountOfUnits) {
        this.amountOfUnits.set(amountOfUnits);
    }
}
