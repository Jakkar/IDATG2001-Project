package no.jakkar.wargames.ui;

import java.io.IOException;
import java.util.HashMap;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * Class for managing scenes and controllers in the applicaion.
 * 
 * Allows for performant and simple switching between scenes without having to
 * create them for each time, and allows easy switching to previous page.
 */
public class SceneManager {

    /**
     * Singleton instance of manager.
     */
    private static SceneManager sceneManagerInstance;

    /**
     * Stage to display stages on.
     */
    private static Stage mainStage;

    /**
     * Collection of loaded stages and their loaders
     * 
     * Consider replacing with a better data structure in the future.
     */
    private static final HashMap<ApplicationScene, Pair<Scene, FXMLLoader>> loadedScenes = new HashMap<>();;

    /**
     * Value for keeping track of currently displayed scene.
     */
    private static ApplicationScene currentScene;

    /**
     * Value for keeping track of previously displayed scene.
     */
    private static ApplicationScene previousScene;

    private SceneManager() {

    }

    /**
     * Lazy method for getting the manager instance.
     * 
     * @return SceneManager instance
     */
    public static SceneManager getInstance() {
        if (sceneManagerInstance == null) {
            sceneManagerInstance = new SceneManager();
        }

        return sceneManagerInstance;
    }

    /**
     * Method for initializing manager with a stage to display scenes on.
     * Must be called before trying to display a scene.
     * 
     * @param stage stage to display scenes on
     */
    public void init(Stage stage) {
        mainStage = stage;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Method for displaying a scene.
     * Will throw exception if .init() has not been called.
     * 
     * @param scene scene to display
     */
    public void displayScene(ApplicationScene scene) {
        // manager must have a stage to work
        if (mainStage == null) {
            throw new IllegalStateException("There is no stage set! Call .init() first!");
        }

        // if scene has not been loaded before, load it now
        if (!loadedScenes.containsKey(scene)) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(scene.getFxmlPath()));
                Parent root = fxmlLoader.load();

                loadedScenes.put(scene, new Pair(new Scene(root), fxmlLoader));
            } catch (IOException e) {
                // should never happen since paths are final in enum but just in case :)
                throw new IllegalArgumentException("Scene fxml not found!");
            }
        }

        // update state and display
        previousScene = currentScene;
        currentScene = scene;
        mainStage.setScene(loadedScenes.get(scene).getKey());
    }

    /**
     * Display the scene that was displayed before the current one.
     * 
     * @throws IllegalArgumentException if there is no previous scene
     */
    public void displayPreviousScene() throws IllegalArgumentException {
        if (previousScene == null) {
            throw new IllegalArgumentException("There is no previous stage!");
        }

        displayScene(previousScene);
    }

    /**
     * Method for getting the loader of a loaded scene.
     * This is used in case access to controller is needed.
     * 
     * @param scene scene to get
     * 
     * @return scene object
     * 
     * @throws IllegalArgumentException thrown if stage is not laoded yet
     */
    public FXMLLoader getSceneLoader(ApplicationScene scene) throws IllegalArgumentException {
        // manager must have a stage to work
        if (mainStage == null) {
            throw new IllegalStateException("There is no stage set! Call .init() first!");
        }

        if (!loadedScenes.containsKey(scene)) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(scene.getFxmlPath()));
                Parent root = fxmlLoader.load();

                loadedScenes.put(scene, new Pair(new Scene(root), fxmlLoader));
            } catch (IOException e) {
                // should never happen since paths are final in enum but just in case :)
                throw new IllegalArgumentException("Scene fxml not found!");
            }
        }

        return loadedScenes.get(scene).getValue();
    }
}
