package no.jakkar.wargames.ui;

/**
 * Enum for keeping track of available scenes and their fxml files.
 */
public enum ApplicationScene {
    START("/no/jakkar/wargames/ui/views/StartPage.fxml"),
    PREPARE_BATTLE("/no/jakkar/wargames/ui/views/CreateNewWar.fxml"),
    CREATE_ARMY("/no/jakkar/wargames/ui/views/CreateNewArmy.fxml"),
    BATTLE("/no/jakkar/wargames/ui/views/Battle.fxml"),
    FINISH("/no/jakkar/wargames/ui/views/BattleResults.fxml");

    private final String fxmlPath;

    private ApplicationScene(String fxmlPath) {
        this.fxmlPath = fxmlPath;
    }

    public String getFxmlPath() {
        return fxmlPath;
    }
}