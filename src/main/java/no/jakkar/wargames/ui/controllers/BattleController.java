package no.jakkar.wargames.ui.controllers;

import java.util.Random;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Color;
import no.jakkar.wargames.model.battle.Battle;
import no.jakkar.wargames.ui.ApplicationScene;
import no.jakkar.wargames.ui.SceneManager;

/**
 * Controller for battle page.
 */
public class BattleController {

    @FXML
    private Button btnResults, btnSkipEnd, btnSkipStart, btnAnimationInfo;

    @FXML
    private Slider sldAnimationStep;

    @FXML
    private ToggleButton tglStartStop;

    @FXML
    private Canvas cnvVisualization;

    /**
     * Field for storing the currently visualized battle.
     */
    private Battle visualizedBattle;

    /**
     * Method called by Javafx.
     * Sets up anything related to GUI not possible/sensible in FXML.
     */
    @FXML
    private void initialize() {
        // update canvas when slider is changed
        sldAnimationStep.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                updateVisualization();
            }
        });
    }

    /**
     * Method for drawing the visualization on the canvas based on slider selection
     */
    private void updateVisualization() {
        GraphicsContext gc = cnvVisualization.getGraphicsContext2D();
        Random rand = new Random();

        gc.clearRect(0, 0, cnvVisualization.getWidth(), cnvVisualization.getHeight());

        int circle1CenterX = (int) cnvVisualization.getWidth() / 3;
        int circle2CenterX = (int) (cnvVisualization.getWidth() / 3) * 2;
        int circlesCenterY = (int) cnvVisualization.getHeight() / 2;

        // radius for area to draw small circles in
        int boundingRadius = 125;

        /// radius for small circles
        int smallCircleRadius = 3;

        // if slider is 0, value will be -1 so just set it to 0 to avoid issues
        int stepValue = (int) sldAnimationStep.getValue() - 1;
        if (stepValue == -1) {
            stepValue = 0;
        }

        gc.setFill(Color.BLUE);

        // TODO: find out why math mess doesn't work :(
        // the random point is in a square instead of a circle

        visualizedBattle.getBattleStates().get(stepValue).getArmyOne().getAllUnits()
                .forEach(unit -> {
                    gc.fillOval(
                            (circle1CenterX + ((boundingRadius * Math.sqrt(rand.nextFloat()))
                                    * Math.cos((rand.nextFloat() * 2 * Math.PI))))
                                    - smallCircleRadius,
                            (circlesCenterY + ((boundingRadius * Math.sqrt(rand.nextFloat()))
                                    * Math.sin((rand.nextFloat() * 2 * Math.PI))))
                                    - smallCircleRadius,
                            smallCircleRadius * 2,
                            smallCircleRadius * 2);
                });

        gc.setFill(Color.RED);

        visualizedBattle.getBattleStates().get(stepValue).getArmyTwo().getAllUnits()
                .forEach(unit -> {
                    gc.fillOval(
                            (circle2CenterX + ((boundingRadius * Math.sqrt(rand.nextFloat()))
                                    * Math.cos((rand.nextFloat() * 2 * Math.PI))))
                                    - smallCircleRadius,
                            (circlesCenterY + ((boundingRadius * Math.sqrt(rand.nextFloat()))
                                    * Math.sin((rand.nextFloat() * 2 * Math.PI))))
                                    - smallCircleRadius,
                            smallCircleRadius * 2,
                            smallCircleRadius * 2);
                });
    }

    /**
     * Method for setting state and switching to results page.
     */
    @FXML
    private void goToResults() {
        // get battle results page controller to set state
        FXMLLoader loader = SceneManager.getInstance().getSceneLoader(ApplicationScene.FINISH);
        BattleResultsController battleResultsController = (BattleResultsController) loader.getController();
        battleResultsController.setVisualizedArmy(visualizedBattle.getVictoriousArmy());

        // switch to battle results page
        SceneManager.getInstance().displayScene(ApplicationScene.FINISH);
    }

    /**
     * Method for setting a new visualized battle.
     * 
     * @param battle battle to visualize
     */
    protected void setVisualizedBattle(Battle battle) {
        this.visualizedBattle = battle;

        if (!this.visualizedBattle.isBattleSimulated()) {
            this.visualizedBattle.simulate();
        }

        sldAnimationStep.setMax(this.visualizedBattle.getBattleStates().size());
        sldAnimationStep.setValue(0);
        updateVisualization();
    }

    /**
     * Method for re-running battle that was previously visualized.
     */
    protected void rerunBattle() {
        // reset the army
        visualizedBattle.reset();

        // set reset to visualized again, as it is not observable and needs to be
        // re-initialized
        setVisualizedBattle(this.visualizedBattle);
    }

    /**
     * Method for showing an infobox with information about the animation.
     */
    @FXML
    private void showControlInfo() {
        StringBuilder str = new StringBuilder();

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setContentText(
                str.append("This animation is a visual representation of the units fighting.")
                        .append(System.lineSeparator())
                        .append("Red and Blue colors represent the two armies, ")
                        .append("and the number of dots correspond to the amount of units alive in the moment shown.")
                        .append(System.lineSeparator())
                        .append("The positions of the dots are random and abitrary, ")
                        .append("and are only chosen to make the animation more interesting.")
                        .append(System.lineSeparator())
                        .append(System.lineSeparator())
                        .append("NOTE: Automatic animation playback is currently WIP, ")
                        .append(" but feel free to manually look through the animation with the slider though :-)")
                        .toString());
        alert.showAndWait();
    }
}
