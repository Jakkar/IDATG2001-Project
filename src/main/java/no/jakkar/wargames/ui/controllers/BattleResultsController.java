package no.jakkar.wargames.ui.controllers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;
import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.units.Unit;
import no.jakkar.wargames.ui.ApplicationScene;
import no.jakkar.wargames.ui.SceneManager;

/**
 * Controller for results page.
 */
public class BattleResultsController {

    @FXML
    private TableColumn<Unit, Integer> tabColUnitHealth;

    @FXML
    private TableColumn<Unit, String> tabColUnitName, tabColUnitType;

    @FXML
    private TableView<Unit> tabUnits;

    @FXML
    private Text txtResultSummary;

    /**
     * Method called by Javafx.
     * Sets up anything related to GUI not possible/sensible in FXML.
     */
    @FXML
    private void initialize() {
        tabColUnitHealth
                .setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getHealth()).asObject());
        tabColUnitName.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getName()));
        tabColUnitType
                .setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getClass().getSimpleName()));
    }

    /**
     * Method for visualizing an army and it's contents.
     * 
     * @param army army to visualize
     */
    protected void setVisualizedArmy(Army army) {
        tabUnits.setItems(FXCollections.observableArrayList(army.getAllUnits()));
        txtResultSummary.setText(
                String.format("Winning Army: %s | Total Remaining Units: %d",
                        army.getName(),
                        army.getAllUnits().size()));
    }

    /**
     * Method for switching to home page.
     */
    @FXML
    private void goToHome() {
        SceneManager.getInstance().displayScene(ApplicationScene.START);
    }

    /**
     * Method for resetting battle state on battle page and switching to it.
     */
    @FXML
    private void rerunSimulation() {
        // get battle page controller to set state
        FXMLLoader loader = SceneManager.getInstance().getSceneLoader(ApplicationScene.BATTLE);
        BattleController battleController = (BattleController) loader.getController();
        battleController.rerunBattle();

        // switch to battle page
        SceneManager.getInstance().displayScene(ApplicationScene.BATTLE);
    }
}
