package no.jakkar.wargames.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import no.jakkar.wargames.ui.ApplicationScene;
import no.jakkar.wargames.ui.SceneManager;

/**
 * Controller for start page.
 */
public class StartPageController {

    /**
     * Method for switching to the createNewWar page.
     */
    @FXML
    private void switchToCreateNewWarPage() {
        SceneManager.getInstance().displayScene(ApplicationScene.PREPARE_BATTLE);
    }

    /**
     * Method for resetting state and switching to createNewArmy page.
     */
    @FXML
    private void switchToCreateNewArmyPage() {
        // get create army page controller to set state
        FXMLLoader loader = SceneManager.getInstance().getSceneLoader(ApplicationScene.CREATE_ARMY);
        CreateNewArmyController battleController = (CreateNewArmyController) loader.getController();
        battleController.clear();

        SceneManager.getInstance().displayScene(ApplicationScene.CREATE_ARMY);
    }
}
