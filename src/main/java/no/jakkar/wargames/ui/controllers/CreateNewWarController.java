package no.jakkar.wargames.ui.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.opencsv.exceptions.CsvValidationException;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import no.jakkar.wargames.model.CSVFileHandler;
import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.army.ArmyRegister;
import no.jakkar.wargames.model.battle.Battle;
import no.jakkar.wargames.model.battle.Terrain;
import no.jakkar.wargames.model.exceptions.InvalidArmyException;
import no.jakkar.wargames.ui.ApplicationScene;
import no.jakkar.wargames.ui.SceneManager;

/**
 * Controller for creating a new war.
 */
public class CreateNewWarController {

    @FXML
    private Button btnCancel, btnStartWar, btnSelectArmy1, btnSelectArmy2, btnImportArmy1, btnImportArmy2;

    @FXML
    private Text txtArmy1, txtArmy2, txtArmy1FileLocation, txtArmy2FileLocation;

    @FXML
    private ComboBox<Terrain> drpTerrain;

    /**
     * Field for storing the first selected army.
     */
    private SimpleObjectProperty<Army> selectedArmy1;

    /**
     * Field for storing the second selected army.
     */
    private SimpleObjectProperty<Army> selectedArmy2;

    public CreateNewWarController() {
        selectedArmy1 = new SimpleObjectProperty<>();
        selectedArmy2 = new SimpleObjectProperty<>();
    }

    /**
     * Method called by Javafx.
     * Sets up anything related to GUI not possible/sensible in FXML.
     */
    @FXML
    private void initialize() {
        btnStartWar.disableProperty().bind(
                Bindings.or(
                        selectedArmy1.isNotNull().not(),
                        selectedArmy2.isNotNull().not()));

        drpTerrain.getItems().setAll(Terrain.values());
        drpTerrain.getSelectionModel().select(0);
    }

    /**
     * Method for opening selection dialog and setting selected army.
     */
    @FXML
    private void selectArmy(ActionEvent event) {
        ArmyRegister.getInstance().loadArmiesFromDefaultLocation();

        if (ArmyRegister.getInstance().getArmies().isEmpty()) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("There are no stored armies!");
            alert.showAndWait();
            return;
        }

        ChoiceDialog<Army> dialog = new ChoiceDialog<>(null, ArmyRegister.getInstance().getArmies());
        dialog.showAndWait().ifPresent(result -> {

            if (((Button) event.getSource()).getId().equals("btnSelectArmy1")) {
                selectedArmy1.set(result);

                txtArmy1FileLocation.visibleProperty().set(false);
                txtArmy1FileLocation.setText("");

                txtArmy1.setText(selectedArmy1.get().getName());
                return;
            }

            if (((Button) event.getSource()).getId().equals("btnSelectArmy2")) {
                selectedArmy2.set(result);

                txtArmy2FileLocation.visibleProperty().set(false);
                txtArmy2FileLocation.setText("");

                txtArmy2.setText(selectedArmy2.get().getName());
                return;
            }

            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("There was an error choosing army!");
            alert.showAndWait();
        });
    }

    /**
     * Method for handling file selection of army.
     */
    @FXML
    private void importArmy(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser
                .getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("CSV Files (.CSV, .csv)", "*.csv", "*.CSV"));
        File file = fileChooser.showOpenDialog(SceneManager.getInstance().getMainStage());

        // if no file was chosen
        if (file == null) {
            return;
        }

        try {
            Army loadedArmy = CSVFileHandler.readArmyFromCSVFile(file);

            if (((Button) event.getSource()).getId().equals("btnImportArmy1")) {
                selectedArmy1.set(loadedArmy);
                txtArmy1FileLocation.setText(String.format("Loaded army in file: %s", file.getAbsolutePath()));
                txtArmy1FileLocation.visibleProperty().set(true);
                txtArmy1.setText(selectedArmy1.get().getName());
            }

            if (((Button) event.getSource()).getId().equals("btnImportArmy2")) {
                selectedArmy2.set(loadedArmy);
                txtArmy2FileLocation.setText(String.format("Loaded army in file: %s", file.getAbsolutePath()));
                txtArmy2FileLocation.visibleProperty().set(true);
                txtArmy2.setText(selectedArmy2.get().getName());
            }

        } catch (FileNotFoundException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Army file not found!");
            alert.showAndWait();
        } catch (IllegalArgumentException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Army file empty!");
            alert.showAndWait();
        } catch (CsvValidationException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Army file malformed!");
            alert.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("There was an error reading the army file!");
            alert.showAndWait();
        } catch (InvalidArmyException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Army data malformed!");
            alert.showAndWait();
        }
    }

    /**
     * Method for starting battle and switching to battle page.
     */
    @FXML
    private void startWar() {
        // create battle with chosen armies
        Battle battle = new Battle(selectedArmy1.get(), selectedArmy2.get(),
                drpTerrain.getSelectionModel().getSelectedItem());
        // run simulation
        // if this turns out to take too long with big armies, add loading anim!
        battle.simulate();

        // get battle page controller to set state
        FXMLLoader loader = SceneManager.getInstance().getSceneLoader(ApplicationScene.BATTLE);
        BattleController battleController = (BattleController) loader.getController();
        battleController.setVisualizedBattle(battle);

        // switch to battle page
        SceneManager.getInstance().displayScene(ApplicationScene.BATTLE);
    }

    /**
     * Method for going back to the previous scene.
     */
    @FXML
    private void goBack() {
        SceneManager.getInstance().displayPreviousScene();
    }

}
