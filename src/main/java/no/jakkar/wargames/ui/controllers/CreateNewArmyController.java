package no.jakkar.wargames.ui.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import no.jakkar.wargames.model.CSVFileHandler;
import no.jakkar.wargames.model.army.Army;
import no.jakkar.wargames.model.units.Unit;
import no.jakkar.wargames.model.units.UnitFactory;
import no.jakkar.wargames.ui.SceneManager;
import no.jakkar.wargames.ui.utils.UnitsTableEntry;

/**
 * Controller for creating an army.
 * 
 * <p>
 * This controller will not actually create an Army object until it finishes.
 * Instead it makes use of a list of UnitsTableEntry objects to keep track of
 * how to create the army. The reasoning for this extra class is that the
 * controller needs to keep track of unit values, if the unit has custom
 * atk/def values and how many of the unit to put in army. The user might want
 * to change these values before saving the army. This is only relevant
 * for this page, and therefore not meaningful to implement in Army class.
 * 
 * @see UnitsTableEntry
 */
public class CreateNewArmyController {

    @FXML
    private Button btnAddUnit, btnCancel, btnDeleteUnit, btnSaveArmy, btnSaveChanges;

    @FXML
    private CheckBox chkUnitCustomStats;

    @FXML
    private ComboBox<String> drpUnitType;

    @FXML
    private TableView<UnitsTableEntry> tabUnits;

    @FXML
    private TableColumn<UnitsTableEntry, Integer> tabColUnitCount;

    @FXML
    private TableColumn<UnitsTableEntry, String> tabColUnitName;

    @FXML
    private TextField txtArmyName, txtUnitArmor, txtUnitAttack, txtUnitHealth, txtUnitName, txtUnitAmount;

    @FXML
    private TextArea txtUnitDesc;

    /**
     * JavaFX style definition for a red background and outline on input fields
     */
    private String ERROR_STYLE = "-fx-background-color: #DBB1B1, #FFF0F0;";

    /**
     * Observable arraylist of units for use with GUI table.
     */
    private ObservableList<UnitsTableEntry> unitList;

    public CreateNewArmyController() {
        unitList = FXCollections.observableArrayList();
    }

    /**
     * Method called by Javafx.
     * Sets up anything related to GUI not possible/sensible in FXML.
     */
    @FXML
    private void initialize() {
        // set the colum widths to equally take up half of table each
        // as this is not easly doable in pure fxml
        tabColUnitName.prefWidthProperty().bind(tabUnits.widthProperty().multiply(0.5));
        tabColUnitCount.prefWidthProperty().bind(tabUnits.widthProperty().multiply(0.5));

        // update unit description when dropdown updates
        drpUnitType.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            txtUnitDesc.setText(
                    UnitFactory.getInstance()
                            .getUnit(newSelection, "a", 1)
                            .getUnitDescription());
        });
        // populate unit type dropdown
        drpUnitType.setItems(FXCollections.observableArrayList(UnitFactory.getAvailableUnits()));
        // default unit is first in the available units list
        drpUnitType.getSelectionModel().select(0);

        // bind form fields disable to if something is selected in table or not
        txtUnitName.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        drpUnitType.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        txtUnitHealth.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        btnSaveChanges.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        chkUnitCustomStats.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        txtUnitDesc.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        txtUnitAmount.disableProperty().bind(tabUnits.getSelectionModel().selectedItemProperty().isNull());
        // attack and armor need to be bound to check button too
        txtUnitAttack.disableProperty().bind(
                Bindings.or(
                        tabUnits.getSelectionModel().selectedItemProperty().isNull(),
                        chkUnitCustomStats.selectedProperty().not()));
        txtUnitArmor.disableProperty().bind(
                Bindings.or(
                        tabUnits.getSelectionModel().selectedItemProperty().isNull(),
                        chkUnitCustomStats.selectedProperty().not()));

        // tell table columns what to display and where to get it
        tabColUnitName.setCellValueFactory(param -> param.getValue().getUnitName());
        tabColUnitCount.setCellValueFactory(param -> param.getValue().getAmountOfUnits().asObject());

        // display unit list in table
        tabUnits.setItems(unitList);

        // add listener on table to update input form with selected unit from list
        tabUnits.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if ((newSelection != oldSelection) && (newSelection != null)) {
                updateInputFormFromTableSelection();
            }
        });
    }

    /**
     * Method for adding a new unit entry to the table.
     */
    @FXML
    private void addNewUnitToList() {
        unitList.add(new UnitsTableEntry());
        tabUnits.getSelectionModel().select(unitList.size() - 1);
    }

    /**
     * Method for removing a unit entry from the table.
     */
    @FXML
    private void removeUnitFromList() {
        int selectionIndex = tabUnits.getSelectionModel().getSelectedIndex();

        // only remove if something is selected
        if (selectionIndex != -1) {
            unitList.remove(selectionIndex);
            // sets selection to same spot as unit was removed from for better UX
            tabUnits.getSelectionModel().select(selectionIndex);
            return;
        }

        Alert alert = new Alert(AlertType.ERROR);
        alert.setContentText("You must select a unit to delete!");
        alert.showAndWait();
    }

    /**
     * Method for updating the input form whenever the user selects a unit from the
     * table.
     */
    @FXML
    private void updateInputFormFromTableSelection() {
        UnitsTableEntry selectedUnit = tabUnits.getSelectionModel().getSelectedItem();

        txtUnitName.setText(selectedUnit.getUnitName().get());
        txtUnitHealth.setText(Integer.toString(selectedUnit.getUnit().getHealth()));
        txtUnitAttack.setText(Integer.toString(selectedUnit.getUnit().getAttack()));
        txtUnitArmor.setText(Integer.toString(selectedUnit.getUnit().getArmor()));
        txtUnitAmount.setText(Integer.toString(selectedUnit.getAmountOfUnits().get()));
        drpUnitType.getSelectionModel().select(selectedUnit.getUnitType().get());

        // not super pretty
        // txtUnitDesc.setText(
        // UnitFactory.getInstance()
        // .getUnit(selectedUnit.getUnitType().get(), "a", 1)
        // .getUnitDescription());
    }

    /**
     * Helper method for validating if a string is an integer or text.
     * 
     * @param input text to validate
     * 
     * @return true if integer, false if not
     */
    private boolean validateIntegerString(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Method for validating the unit form.
     * Will visually highlight invalid fields!
     * 
     * @return true if form is valid, false if form is not
     */
    private boolean validateForm() {
        boolean formValid = true;

        if (txtUnitName.getText().isEmpty()) {
            txtUnitName.setStyle(ERROR_STYLE);
            formValid = false;
        } else {
            txtUnitName.setStyle("");
        }

        if (txtUnitAmount.getText().isEmpty() || !validateIntegerString(txtUnitAmount.getText())
                || (Integer.parseInt(txtUnitAmount.getText()) < 1)) {
            txtUnitAmount.setStyle(ERROR_STYLE);
            formValid = false;
        } else {
            txtUnitAmount.setStyle("");
        }

        if (!validateIntegerString(txtUnitHealth.getText()) || txtUnitHealth.getText().isEmpty()
                || (Integer.parseInt(txtUnitHealth.getText()) < 1)) {
            txtUnitHealth.setStyle(ERROR_STYLE);
            formValid = false;
        } else {
            txtUnitHealth.setStyle("");
        }

        if (!validateIntegerString(txtUnitAttack.getText()) || txtUnitAttack.getText().isEmpty()) {
            txtUnitAttack.setStyle(ERROR_STYLE);
            formValid = false;
        } else {
            txtUnitAttack.setStyle("");
        }

        if (!validateIntegerString(txtUnitArmor.getText()) || txtUnitArmor.getText().isEmpty()) {
            txtUnitArmor.setStyle(ERROR_STYLE);
            formValid = false;
        } else {
            txtUnitArmor.setStyle("");
        }

        return formValid;
    }

    /**
     * Method for updating table data with form.
     * Performs input validation for the various form fields.
     */
    @FXML
    private void handleFormSubmit() {
        if (validateForm()) {
            updateSelectedUnitWithFormData();
        }
    }

    /**
     * Method for updating the selected unit in table with values from form.
     */
    private void updateSelectedUnitWithFormData() {
        UnitsTableEntry selectedUnit = tabUnits.getSelectionModel().getSelectedItem();

        selectedUnit.setUnitName(txtUnitName.getText());
        selectedUnit.setUnitType(drpUnitType.getValue());
        selectedUnit.setUseCustomStats(chkUnitCustomStats.selectedProperty().get());
        // assuming input validation on fields make sure there are only numbers
        selectedUnit.setAmountOfUnits(Integer.parseInt(txtUnitAmount.getText()));
        selectedUnit.getUnit().setHealth(Integer.parseInt(txtUnitHealth.getText()));
        selectedUnit.getUnit().setAttack(Integer.parseInt(txtUnitAttack.getText()));
        selectedUnit.getUnit().setArmor(Integer.parseInt(txtUnitArmor.getText()));
    }

    /**
     * Method for getting army object of current data in table.
     * 
     * @return create army
     */
    private Army getArmy() {
        Army army = new Army(txtArmyName.getText());

        unitList.stream().forEach(entry -> {
            List<Unit> units;
            if (entry.getUseCustomStats().get()) {
                units = UnitFactory
                        .getInstance()
                        .getUnits(
                                entry.getUnitType().get(),
                                entry.getAmountOfUnits().get(),
                                entry.getUnitName().get(),
                                entry.getUnit().getHealth(),
                                entry.getUnit().getArmor(),
                                entry.getUnit().getAttack());
            } else {
                units = UnitFactory
                        .getInstance()
                        .getUnits(
                                entry.getUnitType().get(),
                                entry.getAmountOfUnits().get(),
                                entry.getUnitName().get(),
                                entry.getUnit().getHealth());
            }
            army.addAll(units);
        });

        return army;
    }

    /**
     * Method for checking if the army is valid.
     * Will visually highlight invalid fields!
     * 
     * @return true if valid, false if not
     */
    private boolean validateArmyInput() {
        boolean armyValid = true;

        if (txtArmyName.getText().isEmpty()) {
            txtArmyName.setStyle(ERROR_STYLE);
            armyValid = false;
        } else {
            txtArmyName.setStyle("");
        }

        if (unitList.isEmpty()) {
            tabUnits.setStyle(ERROR_STYLE);
            armyValid = false;
        } else {
            tabUnits.setStyle("");
        }

        return armyValid;
    }

    /**
     * Method for saving the created army to default location.
     */
    @FXML
    private void saveArmy() {
        // stop here if validation failed
        if (!validateArmyInput()) {
            return;
        }

        try {
            CSVFileHandler.writeArmyToCSVFile(
                    Path.of(".", "armystorage", UUID.randomUUID().toString()).toFile(), getArmy());
            SceneManager.getInstance().displayPreviousScene();
        } catch (IOException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("There was an error saving the army!");
            alert.showAndWait();
        }
    }

    /**
     * Method for saving army to user chosen location.
     */
    @FXML
    private void saveArmyToFile() {
        // stop here if validation failed
        if (!validateArmyInput()) {
            return;
        }

        // get save location from user
        FileChooser fileChooser = new FileChooser();
        fileChooser
                .getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("CSV Files (*.CSV)", "*.csv"));
        File file = fileChooser.showSaveDialog(SceneManager.getInstance().getMainStage());

        // if location was chosen, save it
        if (file != null) {
            try {
                CSVFileHandler.writeArmyToCSVFile(file, getArmy());
            } catch (IOException e) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setContentText("There was an error saving the army!");
                alert.showAndWait();
            }
        }
    }

    /**
     * Method for going back to the previous scene.
     */
    @FXML
    private void goBack() {
        SceneManager.getInstance().displayPreviousScene();
    }

    /**
     * Method to clear the unit list.
     */
    protected void clear() {
        unitList.clear();
        txtArmyName.setText("");
    }
}
