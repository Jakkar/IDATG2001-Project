module no.jakkar {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.opencsv;

    opens no.jakkar.wargames.model;
    opens no.jakkar.wargames.model.units;
    opens no.jakkar.wargames.model.battle;
    opens no.jakkar.wargames.model.army;
    opens no.jakkar.wargames.ui.controllers;

    exports no.jakkar.wargames.ui;
    exports no.jakkar.wargames.ui.controllers;

    // For some unknown reason, there is no way to have Javadoc generated for
    // non-exported packages with the maven-javadoc-plugin. In the future, remove
    // these exports if an option for that is added
    exports no.jakkar.wargames.model;
    exports no.jakkar.wargames.model.units;
    exports no.jakkar.wargames.model.battle;
    exports no.jakkar.wargames.model.army;
}
